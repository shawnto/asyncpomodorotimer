import asyncio
import time


class PomodoroSet:
    def __init__(self, username, pomoset=None):
        self.username = username
        self.pomoset = pomoset or {'short_work': 5, 'long_work': 10, 'sw_sets': 2, 'break_time': 2}
        self.pomo_queue = self.create_pomo_queue()
        self.wait_event = asyncio.Event()
        self.wait_event.set()

    def create_pomo_queue(self):
        full_queue = list()
        for n in range(self.pomoset['sw_sets']):
            full_queue += ('short_work', self.pomoset['short_work']), ('break_time', self.pomoset['break_time'])
        full_queue.append(('long_work', self.pomoset['long_work']))
        return full_queue

    async def start_pomo_set(self):
        messages = {
            'short_work': 'You got this!',
            'break_time': 'Take a breather!',
            'long_work': 'Final Stretch!'
        }
        print('starting {} queue'.format(self.username))
        while self.pomo_queue:
            await self.wait_event.wait()
            block_time = self.pomo_queue[0]
            # We want a copy of the true time of the work block,
            # to accurately track when the set is paused.
            session_ticks = block_time[1]
            message_str = '{}\n{}\n{}\n{}'.format('-'*25, block_time,
                                                  '*'*25,
                                                  messages[block_time[0]])
            print(message_str)
            self.pomo_queue = self.pomo_queue[1:]
            start_time = time.time()
            duration = 0.0
            while int(duration) < session_ticks:
                if not self.wait_event.is_set():
                    await self.wait_event.wait()
                    # Update block_time to reflect the pause
                    session_ticks -=  duration
                await asyncio.sleep(0)
                duration = time.time() - start_time
            
        print('finished')
        return '{} finished their set!'.format(self.username)
