import unittest
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from pomodoro_set import PomodoroSet


class TestPomodoroSet(unittest.TestCase):
    '''
    Tests for the PomodoroSet functions
    '''
    def test_set_creation(self):
        pomoset = {'short_work': 5, 'long_work': 10, 'sw_sets': 2, 'break_time': 2}
        result = PomodoroSet('test_user', pomoset=pomoset).pomo_queue
        expected_set = [('short_work', 5), ('break_time', 2),
                        ('short_work', 5), ('break_time', 2),
                        ('long_work', 10)]
        self.assertEqual(result, expected_set)

# TODO more tests on pool manager and pomodoro set


if __name__ == '__main__':
    unittest.main()
        
