import asyncio
import time
from pool_manager import PomodoroPoolManager
from pomodoro_set import PomodoroSet


async def pomo_test_driver(pomo_manager):
    '''
    A temporary substitute for testing to represent the larger event loop this will be a 
    part of.
    '''
    test_set = [PomodoroSet('a'),PomodoroSet('b'),PomodoroSet('c')]
    await asyncio.sleep(2)
    with await pomo_manager.pool_locks['pomos_to_start_lock']:
        for p_set in test_set:
            pomo_manager.pool['pomos_to_start'] = test_set
    await asyncio.sleep(5)
    with await pomo_manager.pool_locks['pomos_to_pause_lock']:
        pomo_manager.pool['pomos_to_pause'].append(test_set[1])
    await asyncio.sleep(10)
    with (await pomo_manager.pool_locks['pomos_to_resume_lock']):
        pomo_manager.pool['pomos_to_resume'].append(test_set[1])
    await asyncio.sleep(10)
    pomo_manager.force_stop_manager = True

        
def main():
    loop = asyncio.get_event_loop()
    pomo_manager = PomodoroPoolManager(loop)
    try:
        loop.run_until_complete(asyncio.gather(pomo_test_driver(pomo_manager), pomo_manager.manage_pool()))
    finally:
        loop.close()


if __name__ == '__main__':
    main()
