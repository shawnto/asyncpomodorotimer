import asyncio


class PomodoroPoolManager:
    '''
    Tracks and manages active Pomodoro sets via a pool of lists. 
    args::
    loop: An async event loop.

    Any functionality added should ensure that it locks when manipulating the pools. 
    '''
    def __init__(self, loop):
        self.pool = {
            'active_pomodoros': [],
            'paused_pomodoros': [],
            'pomos_to_start': [],
            'pomos_to_pause': [],
            'pomos_to_resume': []
        }
        self.pool_locks = {'{}_lock'.format(k): asyncio.Lock() for k, _ in self.pool.items()}
        self.loop = loop
        self.force_stop_manager = False

    async def start_pending_sets(self):
        '''
        Cycle through pomos_to_start and kick off any waiting there.
        '''
        now_active_sets = list()
        with (await self.pool_locks['pomos_to_start_lock']):
            while self.pool['pomos_to_start']:
                pomo_set = self.pool['pomos_to_start'][0] #FIFO
                self.pool['pomos_to_start'] = self.pool['pomos_to_start'][1:]
                self.loop.create_task(pomo_set.start_pomo_set())
                now_active_sets.append(pomo_set)
        with (await self.pool_locks['active_pomodoros_lock']):
            self.pool['active_pomodoros'] += now_active_sets

    async def pause_pending_sets(self):
        '''
        Cycle through pomos_to_pause and clear their event flags to pause them. 
        Then, move them from the active pool and into the paused pool.
        '''
        pausing_sets = list()
        with (await self.pool_locks['pomos_to_pause_lock']):
            while self.pool['pomos_to_pause']:
                pomo_set = self.pool['pomos_to_pause'][0]
                self.pool['pomos_to_pause'] = self.pool['pomos_to_pause'][1:]
                pomo_set.wait_event.clear()
                pausing_sets.append(pomo_set)
            with (await self.pool_locks['active_pomodoros_lock']):
                for pause_set in pausing_sets:
                    self.pool['active_pomodoros'].remove(pause_set)
            with (await self.pool_locks['paused_pomodoros_lock']):
                self.pool['paused_pomodoros'] += pausing_sets

    async def resume_pending_sets(self):
        '''
        Cycle through pomos_to_resume, set their event flags to resume them,
        then place them back in the active queue.
        '''
        with (await self.pool_locks['pomos_to_resume_lock']):
            resumed_pomos = list()
            while self.pool['pomos_to_resume']:
                pomo_set = self.pool['pomos_to_resume'][0]
                self.pool['pomos_to_resume'] = self.pool['pomos_to_resume'][1:]
                pomo_set.wait_event.set()
                resumed_pomos.append(pomo_set)
        with (await self.pool_locks['active_pomodoros_lock']):
            self.pool['active_pomodoros'] += resumed_pomos

    async def manage_pool(self):
        '''
        sub-event loop to manage the pomodoro set queues. 
        Note: We may want to add more 0 sleeps here considering how many
        locks we acquire during this process. But, we'll see after further testing.
        '''
        print('pool started...')
        while True:
            if self.force_stop_manager:
                break
            rest_conditions = [not v for _, v in self.pool.items()]
            if all(rest_conditions):
                await asyncio.sleep(0)
                continue
            if self.pool['pomos_to_start']:
                await self.start_pending_sets()
            if self.pool['pomos_to_pause']:
                await self.pause_pending_sets()
            if self.pool['pomos_to_resume']:
                await self.resume_pending_sets()
            await asyncio.sleep(0)
